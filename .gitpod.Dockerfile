FROM gitpod/workspace-full
RUN sudo apt-get update
RUN DEBIAN_FRONTEND=noninteractive sudo apt install -y neofetch openssh-server wget curl vim nano
RUN DEBIAN_FRONTEND=noninteractive echo 'gitpod:gitpod' | sudo chpasswd